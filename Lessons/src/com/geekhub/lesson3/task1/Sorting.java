package com.geekhub.lesson3.task1;

import java.util.Random;

public class Sorting {
    public static void main(String[] args) {
        Comparable[] elements = new Comparable[10];
        for (int i = 0; i < elements.length; i++) {
            elements[i] = new Person(new Random().nextInt(65));
        }
        Comparable sorted[] = sort(elements);
        for (int i = 0; i < sorted.length; i++) {
            System.out.println(elements[i] + "\t" + sorted[i]);
        }
    }

    public static Comparable[] sort(Comparable[] elements) {
        Comparable[] newElements = new Comparable[elements.length];
        for (int i = 0; i < elements.length; i++) {
            newElements[i] = elements[i];
        }

        for (int i = 0; i < elements.length; i++) {
            for (int j = 0; j < elements.length - 1; j++) {
                if (newElements[j].compareTo(newElements[j + 1]) == 1) {
                    Comparable temp = newElements[j];
                    newElements[j] = newElements[j + 1];
                    newElements[j + 1] = temp;
                }
            }
        }

        return newElements;
    }


}

