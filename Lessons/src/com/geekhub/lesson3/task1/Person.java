package com.geekhub.lesson3.task1;

class Person implements Comparable {
    public int age;

    Person(int age) {
        this.age = age;
    }

    @Override
    public int compareTo(Object o) {
        int age;
        Person anotherPerson = (Person) o;

        if (anotherPerson.age != this.age) {
            if (this.age < anotherPerson.age) {
                return -1;
            } else {
                return 1;
            }
        }
        return 0;
    }

    public String toString() {
        return new String(Integer.toString(this.age));
    }
}

