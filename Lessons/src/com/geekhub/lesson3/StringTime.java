package com.geekhub.lesson3;

public class StringTime {
    public static void main(String[] args) {
        StringTime();
        StringBufferTime();
        StringBuilderTime();
    }

    public static void StringTime() {
        long timeStart = 0;
        long timeEnd = 0;
        String string = new String();
        timeStart = System.currentTimeMillis();
        for (int i = 0; i < 99999; i++) {
            string += string;
        }
        timeEnd = System.currentTimeMillis();
        System.out.println("String time difference: " + (timeEnd - timeStart));
    }

    public static void StringBufferTime() {
        long timeStart = 0;
        long timeEnd = 0;
        StringBuffer stringBufferTime = new StringBuffer();
        timeStart = System.currentTimeMillis();
        for (int i = 0; i < 99999; i++) {
            stringBufferTime.append(stringBufferTime);
        }
        timeEnd = System.currentTimeMillis();
        System.out.println("String Buffer time difference: " + (timeEnd - timeStart));
    }

    public static void StringBuilderTime() {
        long timeStart = 0;
        long timeEnd = 0;
        StringBuilder stringBuilder0 = new StringBuilder();
        timeStart = System.currentTimeMillis();
        for (int i = 0; i < 99999; i++) {
            stringBuilder0.append(stringBuilder0);
        }
        timeEnd = System.currentTimeMillis();
        System.out.println("String Builder time difference: " + (timeEnd - timeStart));
    }
}