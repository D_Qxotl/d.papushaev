package com.geekhub.lesson2;

public class GasTank {
    private int fuel;
    private final int minFuel = 0;
    private int maxFuel = 30;
    GasTank(int maxFuel) {
        this.maxFuel = maxFuel;
    }


    public double getMaxFuel() {
        return maxFuel;
    }

    public double getFuel() {
        return fuel;
    }

    public boolean addFuel(double count) {
        if (count <= 0)
            return false;

        if (fuel + count <= maxFuel) {
            fuel += count;
            return true;
        }

        return false;
    }

    public boolean useFuel(double count) {
        if (count < 0)
            return false;
        if (count > fuel)
            return false;
        fuel -= count;
        return true;
    }
}