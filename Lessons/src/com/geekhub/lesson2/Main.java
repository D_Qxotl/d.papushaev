package com.geekhub.lesson2;

public class Main {
    public static void main(String[] args) {
        FuelCar car = new FuelCar();
        car.turn(-8);
        car.fill(25);
        car.accelerate(12);
        car.brake(7);
        System.out.println(car);
    }
}
