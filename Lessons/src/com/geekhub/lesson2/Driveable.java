package com.geekhub.lesson2;

public interface Driveable {
    public void accelerate(int coefficient);

    public void brake(int coefficient);

    public void turn(int degree);
}
