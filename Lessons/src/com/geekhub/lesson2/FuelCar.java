package com.geekhub.lesson2;

public class FuelCar extends Vehicle {
    private Engine engine;
    private Wheel wheel;
    private GasTank gasTank;

    FuelCar() {
        engine = new Engine(120);
        wheel = new Wheel();
        gasTank = new GasTank(50);
    }

    public void fill(double fuel) {
        gasTank.addFuel(fuel);
    }

    @Override
    public void accelerate(int coefficient) {
        if (gasTank.useFuel(coefficient)) {
            engine.accelerate(coefficient);
            wheel.incSpeed(coefficient);
        }
    }

    @Override
    public void brake(int coefficient) {
        engine.brake(coefficient);
        wheel.decSpeed(coefficient);
    }

    @Override
    public void turn(int degree) {
        wheel.setDegree(degree);
    }

    public String toString() {
        String res = new String("Information about Vehicle\n");
        res += "Speed " + engine.getSpeed() + "\n";
        res += "Fuel " + gasTank.getFuel() + "\n";
        String turning;
        double rotate = wheel.getDegree();
        if (rotate > 0) {
            turning = "Turning right";
        }
        if (rotate < 0) {
            turning = "Turning left";
        } else {
            turning = "Movement directly";
        }
        res += turning + "\n";

        return res;
    }


}
