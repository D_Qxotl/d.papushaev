package com.geekhub.lesson2;

public class Wheel {
    private double degree;
    private double speed;

    public void incSpeed(double incValue) {
        this.speed += incValue;
    }

    public void decSpeed(double decValue) {
        this.speed -= decValue;
    }

    public void setDegree(int degree) {
        this.degree += degree;
    }

    public double getDegree() {
        return degree;
    }

}
