package com.geekhub.lesson2;

abstract public class Vehicle implements Driveable {
    abstract public void accelerate(int coefficient);

    abstract public void brake(int coefficient);

    abstract public void turn(int degree);
}
