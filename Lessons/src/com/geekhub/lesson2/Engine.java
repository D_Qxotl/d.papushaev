package com.geekhub.lesson2;

public class Engine {
    private int speed;
    private int maxSpeed;

    Engine(int maxSpeed) {
        this.maxSpeed = maxSpeed;
    }

    public void accelerate(int accelerationKm) {
        if (speed + accelerationKm < maxSpeed) {
            this.speed += accelerationKm;
        }
    }

    public int getSpeed() {
        return speed;
    }

    public void brake(int brakeKm) {
        if (speed - brakeKm < 0) {
            speed = 0;
        } else {
            speed -= brakeKm;
        }
    }
}
