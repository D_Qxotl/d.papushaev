package com.geekhub.lesson4.task1;

import java.util.HashSet;
import java.util.Set;

public class SetOperationsImpl implements SetOperations {
    @Override
    public boolean equals(Set a, Set b) {
        return a.equals(b);
    }

    @Override
    public Set union(Set a, Set b) {
        Set c = new HashSet();
        c.addAll(a);
        c.addAll(b);
        return c;
    }

    @Override
    public Set subtract(Set a, Set b) {
        Set c = new HashSet();
        c.addAll(a);
        c.removeAll(b);
        return c;
    }

    @Override
    public Set intersect(Set a, Set b) {
        Set с = new HashSet();

        for (Object oneObj : a) {
            if ( b.contains(oneObj) )
                с.add(oneObj);
        }

        return с;
    }

    @Override
    public Set symmetricSubtract(Set a, Set b) {
        return union(subtract(a,b),subtract(b,a));
    }
}
