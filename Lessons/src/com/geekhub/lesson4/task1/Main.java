package com.geekhub.lesson4.task1;

import java.util.HashSet;
import java.util.Set;

public class Main {
    public static void main(String[] args) {
        SetOperations setOperations = new SetOperationsImpl();
        Set a = new HashSet();
        Set b = new HashSet();
        a.add(new String("a1"));
        a.add(new String("a2"));
        a.add(new String("a3"));
        b.add(new String("a1"));
        b.add(new String("a2"));
        b.add(new String("a4"));

        System.out.println("Equals: " + setOperations.equals(a, b));
        System.out.println("Union: " + setOperations.union(a, b));
        System.out.println("Subtract: " + setOperations.subtract(a,b));
        System.out.println("Intersect: " + setOperations.intersect(a,b));
        System.out.println("Symmetric Subtract: " + setOperations.symmetricSubtract(a,b));
    }
}
