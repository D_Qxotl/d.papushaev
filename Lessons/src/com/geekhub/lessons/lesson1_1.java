package com.geekhub.lessons;
import java.util.Scanner;
public class lesson1_1 {
    public static void main(String[] args) {
        int num1 = 1, num2 = 1, sum = 0;
        System.out.print("Введіть n: ");
        Scanner sc = new Scanner(System.in);
        int n = 0;
        try {
            n = sc.nextInt();
            for (int i = 1; i <= n; i++) {
                sum = num1 + num2;
                num1 = num2;
                num2 = sum;
            }
            System.out.println("Результат = " + sum);
        } catch (Exception e) {
            System.out.println("Помилка");
        }


    }
}
