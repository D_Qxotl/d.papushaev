package com.geekhub.lessons;
import java.util.Scanner;
public class Lesson1 {
    public static void main(String[] args){
        int sum = 1;
        Scanner sc = new Scanner(System.in);
        System.out.print("Введіть а: ");
        int a = 0;
        try {
            a = sc.nextInt();
            for (int i=1; i<=a; i++){
                sum*=i;
            }
            System.out.println("Результат = " + sum);
        }
        catch (Exception e) {
            System.out.println("Помилка");
        }
    }

}