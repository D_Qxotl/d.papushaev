package com.geekhub.lesson5.source;

import org.apache.commons.validator.routines.UrlValidator;

import java.io.IOException;
import java.util.ArrayList;
import java.util.List;

/**
 * SourceLoader should contains all implementations of SourceProviders to be able to load different sources.
 */
public class SourceLoader {
    private List<SourceProvider> sourceProviders = new ArrayList<SourceProvider>();

    public SourceLoader() {
        //TODO: initialize me
    }

    public String loadSource(String pathToSource) throws IOException {
        UrlValidator urlValidator = new UrlValidator();
        SourceProvider sourceProvider;
        if (urlValidator.isValid(pathToSource)) {
            sourceProvider = new URLSourceProvider();
        }else{
            sourceProvider = new FileSourceProvider();
        }



        return sourceProvider.load(pathToSource);
    }
}
