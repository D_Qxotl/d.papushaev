package com.geekhub.lesson5.source;

import java.io.*;

/**
 * Implementation for loading content from local file system.
 * This implementation supports absolute paths to local file system without specifying file:// protocol.
 * Examples c:/1.txt or d:/pathToFile/file.txt
 */
public class FileSourceProvider implements SourceProvider {

    @Override
    public boolean isAllowed(String pathToSource) {
        try{
            new FileInputStream(pathToSource);
        }catch (FileNotFoundException e){
            return false;
        }
        return true;
    }

    @Override
    public String load(String pathToSource) throws IOException {
        String text = new String();
        try{
            InputStream inputStream = new FileInputStream(pathToSource);
            byte[] buffer = new byte[1024];
            int len;
            while((len = inputStream.read(buffer)) > 0){
                for(int i = 0; i < len; i++){
                    text += (char)buffer[i];
                }


            }

        }catch (FileNotFoundException e){
            throw e;
        }
        return text;
    }
}
